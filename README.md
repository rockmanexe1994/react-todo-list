# React todo list

## How to run
``` bash

# Install yarn since we will use yarn instead of npm for managing the node_modules
npm i -g yarn

# Install all dependencies
yarn install

# Start the project up
yarn start

```
